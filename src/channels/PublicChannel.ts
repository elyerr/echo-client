import { Channel } from './Channel'

export class PublicChannel extends Channel {

    public class_name: string = "PublicChannel";

}