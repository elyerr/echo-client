import { Channel } from './Channel'

export class PrivateChannel extends Channel {

     public mode: string = "private";

     public class_name: string = "PrivateChannel";
}